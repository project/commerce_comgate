<?php

namespace Drupal\commerce_comgate\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Comgate\Enum\Country;
use Comgate\Enum\Method;
use Comgate\Request\CreatePayment;

/**
 * Provides the class for payment off-site form.
 *
 * Provide a buildConfigurationForm() method which calls buildRedirectForm()
 * with the right parameters.
 */
class ComgateRedirectForm extends BasePaymentOffsiteForm {

  /**
   * Gateway plugin.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
   */
  private $paymentGatewayPlugin;

  /**
   * Getting plugin's configuration.
   *
   * @param string $configuration
   *   Configuration name.
   *
   * @return mixed
   *   Configuration value.
   */
  private function getConfiguration($configuration) {
    return $this->paymentGatewayPlugin->getConfiguration()[$configuration] ?? NULL;
  }

  /**
   * Create URL.
   *
   * @param string $route
   *   The route name.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   Url.
   */
  public function url(string $route) {
    return Url::fromRoute($route, [], ['absolute' => TRUE])->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $order = $payment->getOrder();
    $redirect_url = $this->getConfiguration('comgate_gate_url') . 'create';
    $order_id = $payment->getOrderId();
    $currency_id = $this->getConfiguration('comgate_currency');
    $country = $this->getConfiguration('comgate_country');
    /** @var \Drupal\user\Entity\User $user */
    $user_email = '';
    if ($order_mail = $order->getEmail()) {
      $user_email = $order_mail;
    }
    $total_price_to_comgate = (int) round($order->getTotalPrice()->getNumber() * 100);
    // @todo allow customization and translation of order message
    $payment_msg = 'Order #' . $order->id();
    $createPayment = new CreatePayment($total_price_to_comgate, $order_id, $user_email, $payment_msg);
    if ($country) {
      // Set country from configuration if not blank.
      $createPayment->setCountry($country);
    }
    else {
      // Add fallback country if not set in configuration.
      $createPayment->setCountry(Country::ALL);
    }
    if ($currency_id) {
      // Set currency from configuration if not blank.
      $createPayment->setCurr($currency_id);
    }
    elseif (\Drupal::hasService('commerce_currency_resolver.current_currency')) {
      // Load currency from commerce_currency_resolver module if enabled.
      $createPayment->setCurr(\Drupal::service('commerce_currency_resolver.current_currency')->getCurrency());
    }
    else {
      // Add fallback parameter to avoid missing parameter error.
      $createPayment->setCurr('CZK');
    }
    $current_lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $createPayment->setLang($current_lang);
    // Save language send to comgate to later
    // generate proper url for this language.
    $order->setData('comgate_lang', $current_lang);
    $order->save();
    if ($method = $this->getConfiguration('comgate_method')) {
      // Read allowed method from configuration if not blank.
      $createPayment->setMethod($method);
    }
    else {
      // Allow all methods if not set in configuration.
      $createPayment->setMethod(Method::ALL);
    }
    $data = $createPayment->getData();
    $data += [
      'merchant' => $this->getConfiguration('comgate_merchant_id'),
      'test' => ($this->getConfiguration('mode') == 'sandbox') ? 'true' : 'false',
    ];
    return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, self::REDIRECT_POST);
  }

}
