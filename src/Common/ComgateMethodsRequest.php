<?php

namespace Drupal\commerce_comgate\Common;

use Comgate\Request\RequestInterface;

/**
 *
 */
class ComgateMethodsRequest implements RequestInterface {

  /**
   * @var string
   */
  private $curr;

  /**
   * @var string
   */
  private $country;

  /**
   * @param string|null $curr
   * @param string|null $country
   */
  public function __construct($curr = NULL, $country = NULL) {
    $this->curr = $curr;
    $this->country = $country;
  }

  /**
   * Get currency.
   */
  private function getCurr() {
    return $this->curr;
  }

  /**
   * Get country.
   */
  private function getCountry() {
    return $this->country;
  }

  /**
   * Get data.
   *
   * @return array
   */
  public function getData(): array {
    $data = [];
    if ($this->getCurr()) {
      $data = [
        'curr' => $this->getCurr(),
      ];
    }
    if ($this->getCountry()) {
      $data = [
        'country' => $this->getCountry(),
      ];
    }
    $data['type'] = 'json';
    return $data;
  }

  /**
   * Check if posted.
   *
   * @return bool
   */
  public function isPost(): bool {
    return FALSE;
  }

  /**
   * Check if response is needed to parse.
   *
   * @return bool
   */
  public function needParse() {
    return FALSE;
  }

  /**
   * @return string
   */
  public function getEndPoint(): string {
    return 'methods';
  }

  /**
   * @return string
   */
  public function getResponseClass(): string {
    return ComgateMethodsResponse::class;
  }

}
