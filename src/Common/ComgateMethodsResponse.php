<?php

namespace Drupal\commerce_comgate\Common;

use Comgate\Exception\InvalidArgumentException;

class ComgateMethodsResponse {

  /**
   * @var string
   */
  private $data;

  /**
   * @param string $rawData
   *
   * @throws \Comgate\Exception\InvalidArgumentException
   */
  public function __construct(string $rawData) {
    $parsed_data = $this->parseData($rawData);
    if (isset($parsed_data['error'])) {
      throw new InvalidArgumentException('Error');
    }
    else {
      $this->data = $parsed_data;
    }
  }

  /**
   * @param $rawData
   *
   * @return array
   */
  private function parseData(string $rawData): array {
    $get_json = json_decode($rawData);
    $data = [];
    foreach ($get_json->methods as $value) {
      $data[$value->id] = $value->name . ' | ' . $value->description;
    }
    return $data;
  }

  /**
   * Get Data array.
   *
   * @return array
   */
  public function getData(): array {
    return $this->data;
  }

}
