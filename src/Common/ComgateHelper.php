<?php

namespace Drupal\commerce_comgate\Common;

use Comgate\Client;
use Comgate\Request\CreatePayment;

/**
 * Helper class for Comgate.
 */
class ComgateHelper {

  private $client;

  /**
   * Create new ComgateHelper.
   *
   * @param string $comgate_merchant_id
   * @param bool $mode
   * @param string $secret_code
   */
  public function __construct(string $comgate_merchant_id, bool $mode, string $secret_code) {
    $this->client = new Client($comgate_merchant_id, $mode, $secret_code);
  }

  /**
   * Get methods.
   */
  public function getMethods($curr = NULL, $country = NULL) {
    $getMethods = new ComgateMethodsRequest($curr, $country);
    /** @var \Drupal\commerce_comgate\Common\ComgateMethodsResponse $data */
    $data = $this->client->send($getMethods);
    return $data->getData();
  }

  /**
   * Create payment for comgate.
   *
   * @param array $data
   *
   * @return \Comgate\Response\CreatePaymentResponse|void
   */
  public function createPayment(array $data) {
    try {
      $getMethods = new CreatePayment(...$data);
      return $this->client->send($getMethods);
    }
    catch (\Exception $e) {
      \Drupal::logger('commerce_payment')->error('Comgate payment error @error.',
        ['@error' => $e->getMessage()]);
    }

  }

}
