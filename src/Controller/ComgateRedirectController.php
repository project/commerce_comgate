<?php

namespace Drupal\commerce_comgate\Controller;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentOrderUpdaterInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Comgate\Enum\PaymentStatus;
use Drupal\Core\Language\LanguageInterface;
/**
 * Class ComgateRedirectController.
 */
class ComgateRedirectController extends ControllerBase {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   *   Logger
   */
  private $logger;

  /**
   * @var \Drupal\commerce_payment\PaymentOrderUpdaterInterface
   */
  private $paymentOrderUpdater;

  /**
   * ComgateRedirectController constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Current logger chanel.
   * @param \Drupal\commerce_payment\PaymentOrderUpdaterInterface $payment_order_updater
   */
  public function __construct(LoggerInterface $logger, PaymentOrderUpdaterInterface $payment_order_updater) {
    $this->logger = $logger;
    $this->paymentOrderUpdater = $payment_order_updater;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('commerce_comgate'),
      $container->get('commerce_payment.order_updater')
    );
  }

  /**
   * Result page callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return array
   *   The response.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function result(Request $request) {
    $refId = $request->query->get('refId');
    $transId = $request->query->get('id');
    $result_status = $request->query->get('status');
    if (empty($transId) || empty($refId)) {
      throw new NotFoundHttpException();
    }
    if ($result_status === 'error') {
      return [
        '#markup' => $this->t('The Payment service provider has cancelled the payment'),
      ];
    }
    $payment_storage = $this->entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties([
      'remote_id' => $transId,
      'order_id' => $refId,
    ]);
    $payment = $payments ? reset($payments) : NULL;
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    if ($payment instanceof PaymentInterface && in_array($payment->getRemoteState(), [PaymentStatus::PAID])) {
      $order = $payment->getOrder();
      if ($order instanceof OrderInterface) {
        switch ($payment->getRemoteState()) {
          case PaymentStatus::PAID:
            $this->messenger()->addMessage($this->t('Comgate Payment was processed'));
            break;

          /*  case PaymentStatus::PENDING:
          $this->messenger()->addMessage($this->t('Awaiting transaction completion from the payment service provider, you will be updated once the price has been charged.'));
          break;*/
        }
        throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
          'commerce_order' => $order->id(),
          'step' => 'complete',
        ], [
          // Retrieve save language and generate url.
          'language' => $this->languageManager()->getLanguage($order->getData('comgate_lang') ?? LanguageInterface::LANGCODE_NOT_SPECIFIED),
          'absolute' => TRUE,
        ])->toString());

      }
      else {
        // Log error to Drupal log.
        $this->logger->error('Payment error: <pre>' . $request->getQueryString() . '</pre>');
        // Prints error if payment gateway is missing:
        return [
          '#markup' => $this->t('Something went wrong!'),
        ];
      }
    }
    else {
      // Log error to Drupal log.
      $this->logger->error('Payment error: <pre>' . $request->getQueryString() . '</pre>');
      // Prints error if payment gateway is missing:
      return [
        '#markup' => $this->t('Something went wrong!'),
      ];
    }
  }

  /**
   * Postback page callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function postback(Request $request) {
    $response = $this->validateResponse($request);
    if (!$response) {
      throw new NotFoundHttpException();
    }
    $order_id = $response->refId;
    $order = $this->entityTypeManager()
      ->getStorage('commerce_order')
      ->load($order_id);
    if (!$order instanceof OrderInterface) {
      throw new NotFoundHttpException();
    }
    if (in_array($response->status, [PaymentStatus::PAID])) {
      $this->completePayment($order, $response);
      // Set response message.
      $result = 'code=0&message=OK';
    }
    else {
      $result = 'KO';
      // Log error to Drupal log.
      $this->logger->error('Payment error (server to server request): <pre>' . print_r($response, TRUE) . '</pre>');
    }
    return new Response($result, 200);
  }

  /**
   * Complete new payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   An order.
   * @param object $data
   *   Data from the request.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function completePayment(OrderInterface $order, object $data) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGateway $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->first()->getValue();
    $payment = $this->entityTypeManager()
      ->getStorage('commerce_payment')
      ->create([
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $payment_gateway['target_id'],
        'order_id' => $order->id(),
        'remote_id' => $data->transId,
      ]);
    $payment->setState($data->status === PaymentStatus::PAID ? 'completed' : 'pending');
    $payment->setRemoteState($data->status);
    $payment->save();
    $this->paymentOrderUpdater->requestUpdate($order);
    $this->paymentOrderUpdater->updateOrders();
    $message = $this->t('New payment for order #@order', ['@order' => $order->id()]);
    $this->logger->info($message, [
      'link' => $order->toLink('Order')->toString(),
    ]);
  }

  /**
   * Validate the postback data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return object|null
   */
  private function validateResponse(Request $request): ?object {
    if ($request->getMethod() != 'POST') {
      $this->logger->error($this->t('Wrong response method.'));
      return NULL;
    };
    $data = $request->request->all();
    if (empty($data)) {
      $this->logger->error($this->t('Missing params.'));
      return NULL;
    }
    $params = new \stdClass();
    $params->price = $data['price'] ?? "";
    $params->curr = $data['curr'] ?? "";
    $params->refId = $data['refId'] ?? "";
    $params->status = $data['status'] ?? "";
    $params->transId = $data['transId'] ?? "";
    $params->method = $data['method'] ?? "";
    $params->vs = $data['vs'] ?? "";
    $params->secret = $data['secret'] ?? "";

    if (!is_numeric($params->refId)) {
      $this->logger->error($this->t('refId is not numeric'));
      return NULL;
    }

    if (!is_numeric($params->price)) {
      $this->logger->error($this->t('Price is not numeric'));
      return NULL;
    }

    if (!in_array(strtoupper($params->status), [
      PaymentStatus::PENDING,
      PaymentStatus::PAID,
      PaymentStatus::CANCELLED,
    ])) {
      $this->logger->error($this->t('Unknown status'));
      return NULL;
    }
    $gateway = $this->entityTypeManager()
      ->getStorage('commerce_payment_gateway')->loadByProperties([
        'plugin' => 'comgate_gateway',
      ]);
    $plugin = reset($gateway);
    $plugin = $plugin->getPlugin();
    $sign = $plugin->getConfiguration()['comgate_secret_code'];

    if ($sign !== $params->secret) {
      $this->logger->error($this->t('Secret code does not match'));
      return NULL;
    }
    return $params;
  }

}
