<?php

namespace Drupal\commerce_comgate\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_comgate\Common\ComgateHelper;
use Drupal\Core\Url;

/**
 * Provides the Off-site Redirect payment Comgate gateway.
 *
 * @CommercePaymentGateway(
 *   id = "comgate_gateway",
 *   label = "Comgate (Off-site redirect)",
 *   display_label = "Comgate",
 *   forms = {
 *   "offsite-payment" =
 *   "Drupal\commerce_comgate\PluginForm\OffsiteRedirect\ComgateRedirectForm"
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = { "mastercard", "visa" },
 *   modes = {
 *     "sandbox" = @Translation("Sandbox"),
 *     "live" = @Translation("Live")
 *   },
 * )
 */
class ComgatePaymentGateway extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = [
      'comgate_merchant_id' => '',
      'comgate_secret_code' => '',
      'comgate_country' => '',
      'comgate_currency' => '',
      'comgate_url_ok' => '',
      'comgate_url_notify' => '',
      'comgate_gate_url' => '',
    ];

    return $defaults + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['comgate_merchant_id'] = [
      '#title' => $this->t('Shop connection identifier'),
      '#description' => $this->t('Enter the merchant id. You can find it in the comgate merchant interface.'),
      '#default_value' => $this->configuration['comgate_merchant_id'] ?? '',
      '#type' => 'textfield',
      '#required' => TRUE,
    ];

    $form['comgate_secret_code'] = [
      '#title' => $this->t('Password'),
      '#description' => $this->t('Enter the password. You can find it in the comgate merchant interface'),
      '#default_value' => $this->configuration['comgate_secret_code'] ?? '',
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    $form['comgate_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Preffered country'),
      '#default_value' => $this->configuration['comgate_country'] ?? '',
      '#options' => $this->availableCountries(),
      '#description' => $this->t('Country code ("CZ", "SK"), the parameter is used to limit the selection of payment methods for the specified country.'),
    ];
    $form['comgate_currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Preffered currency'),
      '#default_value' => $this->configuration['comgate_currency'] ?? '',
      '#options' => $this->availableCurrencies(),
      '#description' => $this->t('Filling in the parameter to the values CZK or EUR will return methods that support the specified currency.'),
    ];
    $form['comgate_url_ok'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url paid:'),
      '#default_value' => $this->getComgateUrl('commerce_comgate.comgate_redirect_controller_result', '?id=${id}&refId=${refId}&status=ok'),
      '#disabled' => 'disabled',
      '#description' => $this->t('Copy-paste this into your comgate Merchant (Thank you page). This URL is generated for display and can not be changed.'),
    ];
    $form['comgate_url_cancelled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url cancelled:'),
      '#default_value' => $this->getComgateUrl('commerce_comgate.comgate_redirect_controller_result', '?id=${id}&refId=${refId}&status=cancel'),
      '#disabled' => 'disabled',
      '#description' => $this->t('Copy-paste this into your comgate Merchant. This URL is generated for display and can not be changed.'),
    ];
    $form['comgate_url_pending'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url pending:'),
      '#default_value' => $this->getComgateUrl('commerce_comgate.comgate_redirect_controller_result', '?id=${id}&refId=${refId}&status=pending'),
      '#disabled' => 'disabled',
      '#description' => $this->t('Copy-paste this into your comgate Merchant. This URL is generated for display and can not be changed.'),
    ];
    $form['comgate_url_postback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url for payment result transfer:'),
      '#default_value' => $this->getComgateUrl('commerce_comgate.comgate_redirect_controller_postback'),
      '#disabled' => 'disabled',
      '#description' => $this->t('Copy-paste this into your comgate Merchant. This URL is generated for display and can not be changed.'),
    ];
    $form['comgate_gate_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comgate API url'),
      '#default_value' => $this->configuration['comgate_gate_url'] ?? 'https://payments.comgate.cz/v1.0/',
      '#description' => $this->t('Current Comgate base url.'),
    ];
    if (!empty($this->configuration['comgate_merchant_id']) && !empty($form['comgate_secret_code'])) {
      $form['comgate_method'] = [
        '#type' => 'select',
        '#title' => $this->t('Preffered method'),
        '#options' => $this->availableMethods(),
        '#default_value' => $this->configuration['comgate_method'] ?? '',
        '#description' => $this->t('create the payment with the method parameter'),
      ];
    }
    return $form;
  }

  /**
   * Currencies supported by API.
   *
   * @return array
   *   Currencies
   */
  public function availableMethods(): array {
    $methods[''] = $this->t('All Methods');
    $client = new ComgateHelper($this->configuration['comgate_merchant_id'], $this->configuration['mode'] === 'sandbox', $this->configuration['comgate_secret_code']);
    $get_methods = $client->getMethods($this->configuration['comgate_currency'] ?? NULL, $this->configuration['comgate_country'] ?? NULL);
    if (!empty($get_methods)) {
      $methods += $get_methods;
    }
    return $methods;
  }

  /**
   * Currencies supported by API.
   *
   * @return array
   *   Currencies
   */
  public function availableCurrencies(): array {
    return [
      '' => $this->t('All currencies'),
      'EUR' => $this->t('EURO'),
      'CZK' => $this->t('CZK'),
    ];
  }

  /**
   * Countries list supported by API.
   *
   * @return array
   *   Countries
   */
  public function availableCountries(): array {
    return [
      '' => $this->t('All Countries'),
      'CZ' => $this->t('Česká republika'),
      'SK' => $this->t('Slovensko'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['comgate_merchant_id'] = $values['comgate_merchant_id'];
      $this->configuration['comgate_secret_code'] = $values['comgate_secret_code'];
      $this->configuration['comgate_country'] = $values['comgate_country'];
      $this->configuration['comgate_currency'] = $values['comgate_currency'];
      if (isset($values['comgate_method'])) {
        $this->configuration['comgate_method'] = $values['comgate_method'];
      }
      $this->configuration['comgate_gate_url'] = $values['comgate_gate_url'];
    }
  }

  /**
   * Generate URLs for the Comgate backoffice.
   */
  public function getComgateUrl(string $route, string $query = '') {
    return Url::fromRoute($route, [], ['absolute' => TRUE])->toString() . $query;
  }

}
