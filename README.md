# Drupal Commerce Comgate integration module


## Introduction

Commerce Comgate module allows to accept payments using comgate.cz. Currently, it supports
off-site payment, redirecting the user on comgate.cz page to finish payment.

## Usage

First you have to setup the module on Drupal Commerce:

1.  Install the module.
2.  Enable the module.
3.  Add the payment gateway from `/admin/commerce/config/payment-gateways`.
4.  Configure the payment gateway:
  -   Gateway name. (Usually 'Comgate').
  -   Mode: select **Sandbox** if you want to use the Comgate sandbox, select **Live** otherwise.
  -   Shop connection identifier : Shop connection identifier from the Comgate backoffice (Identifikátor propojení obchodu).
  -   Password: Password (Heslo) from the Comgate backoffice (Like zICCzmDW123BTTMtADzlDViEgTliT7Sw).
  -   Select the preffered country.
  -   Select the currency code.
  -   Copy-paste Url paid, Url cancelled, Url pending and Url for payment result transfer to the Comgate backoffice
  -   Comgate API url should be unchanged.

  **Don't forget to update IP whitelist in your Comgate backoffice - otherwise you aren't able
  to get Payment methods.**

  -  After saving preferences, go to the Gateway settings again and choose *Preffered method*



